package com.dkd.manage.domain;

import lombok.Data;

@Data
public class RegionVo extends Region{
    // 点位数量
    private Integer nodeCount;
}
